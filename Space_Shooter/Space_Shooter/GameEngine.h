#pragma once
#ifndef GAME_ENGINE_H
#define GAME_ENGINE_H
#include "FileSystem.h"
#include "RenderSystem.h"
#include "AssetManager.h"
#include "InputManager.h"
#include "SpawnManager.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "Physics.h"
#include "Time.h"


class GameEngine
{

public:
	inline static GameEngine& Instance()
	{
		static GameEngine instance;
		return instance;
	}

	void Initialize();
	void GameLoop();


	FileSystem* fileSystem;
	RenderSystem* renderSystem;
	AssetManager* assetManager;
	InputManager* inputManager;
	SpawnManager* spawnManager;
	GameObjectManager* gameObjectManager;
private:
	explicit GameEngine();
	~GameEngine();
	inline explicit GameEngine(GameEngine const&) = delete;
	inline GameEngine& operator=(GameEngine const&) = delete;	
};

#endif