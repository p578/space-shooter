#pragma once
#ifndef ENEMY1_H
#define ENEMY1_H
#include "SpaceShooter.h"
#include "Player.h"
#include "Hud.h"
class Enemy:public GameObject
{
public:
	Enemy();
	Enemy(int _enemyType, sf::Vector2f _position);
	~Enemy();

	void Initialize();
	void Update(); //keep distance and attack player(translate, fire), query collision(take damage), health = 0(die and explode)
	void SaveGame(json::JSON& _doc);
	void LoadGame(json::JSON& _doc);
	void LoadSettings(json::JSON& _doc);

	//state variables
	bool fire;
	bool destory;
	sf::Vector2f heading;
private:
	int health;
	float fireTimer;
	float selfDestructTimer;
	int enemyType;
	bool attackMode;

	//attribute variables
	int maxHealth;
	float fireInterval;
	float speed;
	float driftSpeed;


	void OnCollision(GameObject* _var);
	void TakeDamage(int _damage);
	sf::Vector2f TrackPlayer();
	float RangePlayer();
	void Fire();
	void Die();

	
};

#endif 


