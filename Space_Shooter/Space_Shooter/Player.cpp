#include "Player.h"

void Player::Initialize()
{
	//attribute variable
	maxPlayerLife = 3;
	maxHealth = 2;
	speed = 150;
	radius = 25;
	fireInterval = 0.2f;
	driftSpeed = 50;
	driftRotation = 50;
	tag = "player";

	//state ariable
	playerLife = maxPlayerLife;
	health = maxHealth;
	reviveDelay = 1;
	alive = true;
	explode = false;
	fire = false;
	fireTimer = 0.5f;

	sf::Sprite* sprite = new sf::Sprite();
	std::string textureKey = "player";
	const sf::Texture* texture = AssetManager::GetTexture(textureKey);
	textures.push_back(texture);
	sprite->setTexture(*texture);
	transform = sf::Vector2f(0,200);
	sprite->setPosition(transform);
	sprite->setOrigin(sf::Vector2f(texture->getSize().x * 0.5f, texture->getSize().y * 0.5f));
	sprites.push_back(sprite);
	textureKey = "playerDamaged";
	texture = AssetManager::GetTexture(textureKey);
	textures.push_back(texture);
	textureKey = "playerDamaged_2";
	texture = AssetManager::GetTexture(textureKey);
	textures.push_back(texture);

}

void Player::Update()
{
	if (alive) {
		if (InputManager::GetKey("W"))
		{
			sprites.front()->move(sf::Vector2f(0, -1) * speed * Time::deltaTime);
			transform = sprites.front()->getPosition();
			if (transform.y <= -380)
			{
				transform.y = -380;
				sprites.front()->setPosition(transform);
			}
		}
		if (InputManager::GetKey("A"))
		{
			sprites.front()->move(sf::Vector2f(-1, 0) * speed * Time::deltaTime);
			transform = sprites.front()->getPosition();
			if (transform.x <= -250)
			{
				transform.x = -250;
				sprites.front()->setPosition(transform);
			}
		}
		if (InputManager::GetKey("S"))
		{
			sprites.front()->move(sf::Vector2f(0, 1) * speed * Time::deltaTime);
			transform = sprites.front()->getPosition();
			if (transform.y >= 380)
			{
				transform.y = 380;
				sprites.front()->setPosition(transform);
			}
		}
		if (InputManager::GetKey("D"))
		{
			sprites.front()->move(sf::Vector2f(1, 0) * speed * Time::deltaTime);
			transform = sprites.front()->getPosition();
			if (transform.x >= 250)
			{
				transform.x = 250;
				sprites.front()->setPosition(transform);
			}
		}

		if (InputManager::GetKey("Space"))
		{
            Fire();
		}

		for (GameObject* var:collision)
		{
			OnCollision(var);
		}
		collision.clear();

		if (health <= 0)
		{
			explode = true;
			alive = false;
			playerLife--;
		}
	}
	else
	{
		if (playerLife > 0)// Input.GetKey("continue")
		{
			if (reviveDelay > 0)
			{
				reviveDelay -= Time::deltaTime;
				Die();
			}
			else
			{
				Reset();
			}
		}
		else
		{
			Die();
		}
	}
}

void Player::OnCollision(GameObject* _var)
{
	if (_var->tag == "bigAsteroid")
	{
		Hud::Instance().point += 2;
		TakeDamage(2);
	}	
	else if (_var->tag == "smallAsteroid")
	{
		Hud::Instance().point++;
		TakeDamage(1);
	}
	else if (_var->tag == "enemy")
	{
		Hud::Instance().point += 2;
		TakeDamage(2);
	}	
	else if (_var->tag == "enemyProjectile")
	{
		TakeDamage(1);
	}	
}

void Player::TakeDamage(int _damage)
{
	health -= _damage;
	if (health <= maxHealth / 2)
	{
		sprites.front()->setTexture(*textures[1]);
	}
}

void Player::Fire()
{
	if (fireTimer >= fireInterval)
	{
		fire = true;
		fireTimer = 0;
	}
	fireTimer += Time::deltaTime;
}

void Player::Die()
{
	sprites.front()->setTexture(*textures[2]);
	sprites.front()->move(sf::Vector2f(0,1)*driftSpeed*Time::deltaTime);
	sprites.front()->rotate(driftRotation*Time::deltaTime);
	transform = sprites.front()->getPosition();
	if (transform.y > 500)
	{
		transform.y = 500;
		sprites.front()->setPosition(transform);
	}
}

void Player::Reset()
{
	health = maxHealth;
	reviveDelay = 1;
	alive = true;
	explode = false;
	fire = false;
	fireTimer = 0.5f;
	transform = sf::Vector2f(0, 200);
	sprites.front()->setPosition(transform);
	sprites.front()->setTexture(*textures[0]);
	sprites.front()->setRotation(0);
}

void Player::SaveGame(json::JSON& _doc)
{
	json::JSON Player = json::JSON::Object();
	Player["playerLife"] = playerLife;
	Player["fire"] = fire;
	Player["explode"] = explode;
	Player["alive"] = alive;
	Player["health"] = health;
	Player["fireTimer"] = fireTimer;
	Player["reviveDelay"] = reviveDelay;
	GameObject::SaveGame(Player);
	_doc["Player"] = Player;
}

void Player::LoadGame(json::JSON& _doc)
{
	if (_doc.hasKey("Player"))
	{
		json::JSON player = _doc["Player"];
		playerLife = player["playerLife"].ToInt();
		fire = player["fire"].ToBool();
		explode = player["explode"].ToBool();
		alive = player["alive"].ToBool();
		health = player["health"].ToInt();
		fireTimer = player["fireTimer"].ToFloat();
		reviveDelay = player["reviveDelay"].ToFloat();
		GameObject::LoadGame(player);
	}
}

void Player::LoadSettings(json::JSON& _doc)
{
}