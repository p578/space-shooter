#pragma once
#ifndef ASTEROID_H
#define ASTEROID_H

#include "SpaceShooter.h"
#include "Hud.h"
class Asteroid : public GameObject
{

public:
	Asteroid();
	Asteroid(int _asteroidType, sf::Vector2f _position, float _initialAngle);
	~Asteroid();

	void Initialize() override;
	void Update() override;

	void SaveGame(json::JSON& _doc);
	void LoadGame(json::JSON& _doc);
	void LoadSettings(json::JSON& _doc);
	//state variable
	bool destory;

private:
	int asteroidType;

	//attribute variable
	sf::Vector2f driftSpeed;
	float rotationSpeed;
	float initialAngle;
};

#endif

