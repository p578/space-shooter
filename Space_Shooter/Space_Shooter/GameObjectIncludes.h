#pragma once
#ifndef GAME_OBJECT_INCLUDES_H
#define GAME_OBJECT_INCLUDES_H
#include "GameObject.h"
#include "BackGround.h"
#include "Star.h"
#include "Asteroid.h"
#include "Player.h"
#include "Enemy.h"
#include "Projectile.h"
#include "LasorShot.h"
#include "Explosion.h"
#include "Hud.h"
#endif // !GAME_OBJECT_INCLUDES_H
