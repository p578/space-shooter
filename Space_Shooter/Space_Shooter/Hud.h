#pragma once
#ifndef HUD_H
#define HUD_H
#include "SpaceShooter.h"
#include "Player.h"
class Hud : public GameObject
{
public:
	inline static Hud& Instance()
	{
		static Hud instance;
		return instance;
	}
private:
	inline explicit Hud() { }
	inline ~Hud() { }
	inline explicit Hud(Hud const&) = delete;
	inline Hud& operator=(Hud const&) = delete;

public:
	int point;
	int highScore;

	void Initialize();
	void Update();
	void FillLife();
	void SaveGame(json::JSON& _doc);
	void LoadGame(json::JSON& _doc);
	void LoadSettings(json::JSON& _doc);

private:
	void ShowGameOver();
	void ClearGameOver();
	void DeductLife();
	

};

#endif // !HUD_H

