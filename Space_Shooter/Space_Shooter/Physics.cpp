#include "Physics.h"

void Physics::Initialize()
{
}

void Physics::Update()
{
	std::list<GameObject*>::iterator it1;
	std::list<GameObject*>::iterator it2;
	for (it1 = GameObjectManager::gameObjects.begin(); it1 != GameObjectManager::gameObjects.end();it1++)
	{
		for (it2 = next(it1,1); it2 != GameObjectManager::gameObjects.end();it2++)
		{
			GameObject* g1 = *it1;
			GameObject* g2 = *it2;

			sf::Vector2f l = g1->transform - g2->transform;
			double distance = sqrt(pow(l.x, 2) + pow(l.y, 2));
			if (distance <= (double)g1->radius + (double)g2->radius)
			{
				g1->collision.push_back(g2);
				g2->collision.push_back(g1);
			}
		}
	}
}