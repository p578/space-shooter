#pragma once
#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H
#include "AssetManager.h"
#include "InputManager.h"
#include "json.hpp"
#include <list>
class GameObject
{
public:
	GameObject();
	virtual ~GameObject();

	virtual void Initialize() {}
	virtual void Update() {}
	void SaveGame(json::JSON& _doc);
	void LoadGame(json::JSON& _doc);
	void LoadSettings(json::JSON& _doc);
	//attribute variable
	std::list<sf::Sprite*> sprites;
	std::list<sf::Text*> texts;
	std::string tag;
	float radius;
	
	//state variable
	std::list<GameObject*> collision;
	sf::Vector2f transform;
	
protected:
	//attribute varibale
	 std::vector<const sf::Texture*> textures;//reference assets in the assetManager
	 std::vector<const sf::Font*> fonts;//reference assets in the assetManager
};

#endif


