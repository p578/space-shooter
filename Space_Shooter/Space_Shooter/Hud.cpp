#include "Hud.h"

void Hud::Initialize()
{
	highScore = 0;
	point = 0;
	FillLife();

	sf::Text* text = new sf::Text();
	std::string fontKey = "cour";
	const sf::Font* font = AssetManager::GetFont(fontKey);
	text->setFont(*font);
	text->setString("Points: 0");
	text->setPosition(sf::Vector2f(150,-350));
	text->setCharacterSize(15);
	texts.push_back(text);
}

void Hud::Update()
{
	if (Player::Instance().playerLife < sprites.size())
	{
		DeductLife();
		if (Player::Instance().playerLife == 0)
		{
			ShowGameOver();
		}
	}

	texts.front()->setString("Points: " + std::to_string(point));
}

void Hud::FillLife()
{
	sf::Sprite* sprite = new sf::Sprite();
	std::string textureKey = "life";
	const sf::Texture* texture = AssetManager::GetTexture(textureKey);
	sprite->setTexture(*texture);
	sprite->setPosition(sf::Vector2f(-230, -350));
	sprites.push_back(sprite);

	sprite = new sf::Sprite();
	textureKey = "life";
	texture = AssetManager::GetTexture(textureKey);
	sprite->setTexture(*texture);
	sprite->setPosition(sf::Vector2f(-204, -350));
	sprites.push_back(sprite);

	sprite = new sf::Sprite();
	textureKey = "life";
	texture = AssetManager::GetTexture(textureKey);
	sprite->setTexture(*texture);
	sprite->setPosition(sf::Vector2f(-178, -350));
	sprites.push_back(sprite);

	ClearGameOver();
}

void Hud::DeductLife()
{
	sprites.pop_back();
}

void Hud::ShowGameOver()
{
	if (point > highScore)
		highScore = point;

	sf::Text* text = new sf::Text();
	std::string fontKey = "cour";
	const sf::Font* font = AssetManager::GetFont(fontKey);
	text->setFont(*font);
	text->setString("Game Over");
	text->setPosition(sf::Vector2f(-70,-50));
	text->setCharacterSize(30);
	texts.push_back(text);

	text = new sf::Text();
	fontKey = "cour";
	font = AssetManager::GetFont(fontKey);
	text->setFont(*font);
	text->setString("High Score: " + std::to_string(highScore));
	text->setPosition(sf::Vector2f(-65, 0));
	text->setCharacterSize(20);
	texts.push_back(text);

	text = new sf::Text();
	fontKey = "cour";
	font = AssetManager::GetFont(fontKey);
	text->setFont(*font);
	text->setString("Press R to restart");
	text->setPosition(sf::Vector2f(-95, 50));
	text->setCharacterSize(20);
	texts.push_back(text);
}

void Hud::ClearGameOver()
{
	point = 0;
	if (texts.size() == 4)
	{
		texts.pop_back();
		texts.pop_back();
		texts.pop_back();
	}
}

void Hud::SaveGame(json::JSON& _doc)
{
	json::JSON hud = json::JSON::Object();
	hud["point"] = point;
	hud["highScore"] = highScore;
	_doc["hud"] = hud;
}

void Hud::LoadGame(json::JSON& _doc)
{
	if (_doc.hasKey("hud"))
	{
		json::JSON hud = _doc["hud"];
		point = hud["point"] .ToInt();
		highScore = hud["highScore"] .ToInt();
	}
}

void Hud::LoadSettings(json::JSON& _doc)
{
}