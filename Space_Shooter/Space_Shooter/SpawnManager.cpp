#include "SpawnManager.h"
#include <iostream>
SpawnManager::SpawnManager()
{
	spawnPointIndex = 0;
	starSpawnTimer = 0;
	starSpawnInterval = 0;
	asteroidSpawnTimer = 0;
	asteroidSpawnInterval = 0;
	enemySpawnTimer = 0;
	enemySpawnInterval = 0;
	backGround = new BackGround();
	player = &Player::Instance();
	hud = &Hud::Instance();
}

SpawnManager::~SpawnManager()
{
	GameObjectManager::RemoveBackGroundElement(backGround);
	delete backGround;
	GameObjectManager::RemoveGameObject(player);
	GameObjectManager::RemoveBackGroundElement(hud);

	for (auto& var : stars)
	{
		if (var != nullptr)
		{
			GameObjectManager::RemoveBackGroundElement(var);
			delete var;
		}
	}
	for (auto& var : asteroids)
	{
		if (var != nullptr)
		{
			GameObjectManager::RemoveGameObject(var);
			delete var;
		}
	}
	for (auto& var : projectiles)
	{
		if (var != nullptr)
		{
			GameObjectManager::RemoveGameObject(var);
			delete var;
		}
	}
	for (auto& var : lasorShots)
	{
		if (var != nullptr)
		{
			GameObjectManager::RemoveGameObject(var);
			delete var;
		}
	}
	for (auto& var : explosions)
	{
		if (var != nullptr)
		{
			GameObjectManager::RemoveGameObject(var);
			delete var;
		}
	}
	for (auto& var : enemies)
	{
		if (var != nullptr)
		{
			GameObjectManager::RemoveGameObject(var);
			delete var;
		}
	}	
}

void SpawnManager::Initialize()
{
	//Load from JSON
	spawnPointIndex = 0;
	starSpawnTimer = 0;
	starSpawnInterval = 1;
	asteroidSpawnTimer = 0;
	asteroidSpawnInterval = 3;
	enemySpawnTimer = 0;
	enemySpawnInterval = 3;

	//BackGround
	GameObjectManager::AddBackGroundElement(backGround);
	//Player
	GameObjectManager::AddGameObject(player);
	//Hud
	GameObjectManager::AddBackGroundElement(hud);
	//Spawn Points
	srand(time(NULL));


	for (int i = 0; i < 42; i++)
	{
		if (i < 14)
		{
			spawnPoints.push_back(sf::Vector2f(45 * (i - 7), 0));
		}
		else if (i >= 14 && i < 28)
		{
			spawnPoints.push_back(sf::Vector2f(45 * (i - 19), -200));
		}
		else
		{
			spawnPoints.push_back(sf::Vector2f(45 * (i - 31), -400));
		}
	}

	//Stars
	for (int i = 0; i < 5; i++)
	{
		RandomSpawnPoint(0,28);
		SpawnStar(spawnPointIndex);
	}
	//Asteroids
	for (int i = 0; i < 5; i++)
	{
		RandomSpawnPoint(0, 14);
		SpawnAsteroid(spawnPointIndex * 2+14);
	}
}

void SpawnManager::Update()
{
	//stars
	if (starSpawnTimer > starSpawnInterval)
	{
		RandomSpawnPoint(28, 42);
		SpawnStar(spawnPointIndex);
		starSpawnTimer = 0;
	}
	else
	{
		starSpawnTimer += Time::deltaTime;
	}

	//destory stars
	for (auto it = stars.begin();it!= stars.end();)
	{
		Star* temp = *it;
		if (temp->transform.y > 400)
		{
			it = RemoveStar(it);
		}
		else
		{
			it++;
		}
	}

	//Asteroids
	if (asteroidSpawnTimer > asteroidSpawnInterval)
	{
		RandomSpawnPoint(0, 7);
		SpawnAsteroid(spawnPointIndex*2+28);
		asteroidSpawnTimer = 0;
	}
	else
	{
		asteroidSpawnTimer += Time::deltaTime;
	}

	//destory asteroids
	for (auto it = asteroids.begin(); it != asteroids.end();)
	{
		Asteroid* temp = *it;
		if (temp->destory)
		{
			Explosion* exp = new Explosion(1,temp->transform);
			explosions.push_back(exp);
			GameObjectManager::AddGameObject(exp);
			it = RemoveAsteroid(it);
		}
		else
		{
			it++;
		}
	}

	//enemy
	//Do not spawn if game's over
	if (player->playerLife != 0)
	{
		if (enemySpawnTimer > enemySpawnInterval)
		{
			RandomSpawnPoint(0, 7);
			SpawnEnemy(spawnPointIndex * 2 + 28);
			enemySpawnTimer = 0;
		}
		else
		{
			enemySpawnTimer += Time::deltaTime;
		}
	}

	//handle enemy flags
	for (auto it = enemies.begin(); it != enemies.end();)
	{
		Enemy* temp = *it;

		if (temp->fire)
		{
			Projectile* proj = new Projectile(1, temp->transform, temp->heading);
			projectiles.push_back(proj);
			GameObjectManager::AddGameObject(proj);
			temp->fire = false;
		}
		//destory enemy
		if (temp->destory)
		{
			Explosion* exp = new Explosion(1, temp->transform);
			explosions.push_back(exp);
			GameObjectManager::AddGameObject(exp);
			it = RemoveEnemy(it);
		}
		else
		{
			it++;
		}
	}

	//handle player flags
	if (player->fire)
	{
		Projectile* proj = new Projectile(0,player->transform,sf::Vector2f(0,-1));
		projectiles.push_back(proj);
		GameObjectManager::AddGameObject(proj);
		player->fire = false;
	}

	if (player->explode)
	{
		Explosion* exp = new Explosion(0, player->transform);
		explosions.push_back(exp);
		GameObjectManager::AddGameObject(exp);
		player->explode = false;
	}

	//destory projectiles
	for (auto it = projectiles.begin(); it!= projectiles.end();)
	{
		Projectile* proj = *it;
		if (proj->destory)
		{
			GameObjectManager::RemoveGameObject(proj);
			it = projectiles.erase(it);
			LasorShot* shot = new LasorShot(proj->projectileScource,proj->transform);
			delete proj;
			lasorShots.push_back(shot);
			GameObjectManager::AddGameObject(shot);
		}
		else
		{
			it++;
		}
	}
	//destory lasor shot FX
	for (auto it = lasorShots.begin(); it != lasorShots.end();)
	{
		LasorShot* shot = *it;
		if (shot->destory)
		{
			GameObjectManager::RemoveGameObject(shot);
			it = lasorShots.erase(it);
			delete shot;
		}
		else
		{
			it++;
		}
	}

	//destory explosions
	for (auto it = explosions.begin(); it != explosions.end();)
	{
		Explosion* exp = *it;
		if (exp->destory)
		{
			GameObjectManager::RemoveGameObject(exp);
			it = explosions.erase(it);
			delete exp;
		}
		else
		{
			it++;
		}
	}

	if (player->playerLife == 0)
	{
		if (InputManager::GetKey("R"))
		{
			Restart();
		}
	}
}



void SpawnManager::RandomSpawnPoint(int _min, int _max)
{
	spawnPointIndex = rand() % (_max-_min) + _min;
}

void SpawnManager::SpawnStar(int _index)
{
	int starType = rand() % 2;
	Star* temp = new Star(starType, spawnPoints[_index]);
	stars.push_back(temp);
	GameObjectManager::AddBackGroundElement(temp);
}

std::list<Star*>::iterator SpawnManager::RemoveStar(std::list<Star*>::iterator _it)
{
	std::list<Star*>::iterator it;
	GameObjectManager::RemoveBackGroundElement(*_it);
	delete* _it;
	return it = stars.erase(_it);
}

void SpawnManager::SpawnAsteroid(int _index)
{
	int asteroidType = rand() % 2;
	float intialAngle = rand() % 360;
	Asteroid* temp = new Asteroid(asteroidType, spawnPoints[_index],intialAngle);
	asteroids.push_back(temp);
	GameObjectManager::AddGameObject(temp);
}

std::list<Asteroid*>::iterator SpawnManager::RemoveAsteroid(std::list<Asteroid*>::iterator _it)
{
	std::list<Asteroid*>::iterator it;
	GameObjectManager::RemoveGameObject(*_it);
	delete* _it;
	return it = asteroids.erase(_it);
}

void SpawnManager::SpawnEnemy(int _index)
{
	int EnemyType = rand() % 2;
	Enemy* temp = new Enemy(EnemyType, spawnPoints[_index]);
	enemies.push_back(temp);
	GameObjectManager::AddGameObject(temp);
}

std::list<Enemy*>::iterator SpawnManager::RemoveEnemy(std::list<Enemy*>::iterator _it)
{
	std::list<Enemy*>::iterator it;
	GameObjectManager::RemoveGameObject(*_it);
	delete* _it;
	return it = enemies.erase(_it);
}

void SpawnManager::Restart()
{
	//Update Hud
	hud->FillLife();
	//clear enemy
	for (auto it = enemies.begin(); it != enemies.end();)
	{
		it = RemoveEnemy(it);
	}

	//clear asteroids
	for (auto it = asteroids.begin(); it != asteroids.end();)
	{
		it = RemoveAsteroid(it);
	}

	//clear projectiles
	for (auto it = projectiles.begin(); it != projectiles.end();)
	{
		Projectile* proj = *it;
		GameObjectManager::RemoveGameObject(proj);
		it = projectiles.erase(it);
		delete proj;
	}
	//reset player
	player->Reset();
	player->playerLife = player->maxPlayerLife;
}

void SpawnManager::clearInteractables()
{
	for (auto& var : asteroids)
	{
		if (var != nullptr)
		{
			GameObjectManager::RemoveGameObject(var);
			delete var;
		}
	}
	asteroids.clear();

	for (auto& var : projectiles)
	{
		if (var != nullptr)
		{
			GameObjectManager::RemoveGameObject(var);
			delete var;
		}
	}
	projectiles.clear();

	for (auto& var : enemies)
	{
		if (var != nullptr)
		{
			GameObjectManager::RemoveGameObject(var);
			delete var;
		}
	}
	enemies.clear();
}

void SpawnManager::SaveGame(json::JSON& _doc)
{
	json::JSON spawnManager = json::JSON::Object();

	player->SaveGame(spawnManager);
	hud->SaveGame(spawnManager);

	json::JSON asteroidList = json::JSON::Array();
	for (auto& var : asteroids)
	{
		if (var != nullptr)
		{
			var->SaveGame(asteroidList);
		}
	}
	spawnManager["Asteroids"] = asteroidList;

	json::JSON projectileList = json::JSON::Array();
	for (auto& var : projectiles)
	{
		if (var != nullptr)
		{
			var->SaveGame(projectileList);
		}
	}
	spawnManager["Projectiles"] = projectileList;

	json::JSON enemyList = json::JSON::Array();
	for (auto& var : enemies)
	{
		if (var != nullptr)
		{
			var->SaveGame(enemyList);
		}
	}
	spawnManager["Enemies"] = enemyList;

	_doc["SpawnManager"] = spawnManager;
}

void SpawnManager::LoadGame(json::JSON& _doc)
{
	if (_doc.hasKey("SpawnManager"))
	{
		json::JSON spawnManager = _doc["SpawnManager"];
		player->LoadGame(spawnManager);
		hud->LoadGame(spawnManager);
		clearInteractables();
		if (spawnManager.hasKey("Asteroids"))
		{
			json::JSON asteroidList = spawnManager["Asteroids"];
			for (auto& var : asteroidList.ArrayRange())
			{
				Asteroid* temp = new Asteroid();
				temp->LoadGame(var);
				asteroids.push_back(temp);
				GameObjectManager::AddGameObject(temp);
			}
		}
		
		if (spawnManager.hasKey("Projectiles"))
		{
			json::JSON projectileList = spawnManager["Projectiles"];
			for (auto& var : projectileList.ArrayRange())
			{
				Projectile* temp = new Projectile();
				temp->LoadGame(var);
				projectiles.push_back(temp);
				GameObjectManager::AddGameObject(temp);
			}
		}

		if (spawnManager.hasKey("Enemies"))
		{
			json::JSON enemyList = spawnManager["Enemies"];
			for (auto& var : enemyList.ArrayRange())
			{
				Enemy* temp = new Enemy();
				temp->LoadGame(var);
				enemies.push_back(temp);
				GameObjectManager::AddGameObject(temp);
			}
		}
	}
	else
	{
		std::cout << "error loading spawn manager" << std::endl;
	}
}

void SpawnManager::LoadSettings(json::JSON& _doc)
{
}