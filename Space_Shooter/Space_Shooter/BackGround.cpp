#include "BackGround.h"
#include <stdlib.h>     
#include <time.h>      

BackGround::BackGround()
{
}

BackGround::~BackGround()
{
}

void BackGround::Initialize()
{

	srand(time(NULL));
	int randomTexture = 0;
	for (int i = 0; i < 6; i++)
	{
		randomTexture = rand() % 2;
		std::string textureKey;
		if (randomTexture)
		{
			textureKey = "backgroundColor";
		}
		else
		{
			textureKey = "starBackground";
		}
		sf::Sprite* temp = new sf::Sprite();
		temp->setTexture(*AssetManager::GetTexture(textureKey));
		temp->setOrigin(SPRITE_SIZE/2, SPRITE_SIZE / 2);
		if (i < 3)
		{
			temp->setPosition(sf::Vector2f(-SPRITE_SIZE/2, i * SPRITE_SIZE - 1 * SPRITE_SIZE));
		}
		else
		{
			temp->setPosition(sf::Vector2f(SPRITE_SIZE / 2, (i-3) * SPRITE_SIZE - 1 * SPRITE_SIZE));
		}
		randomTexture = rand() % 4;
		temp->setRotation(randomTexture * 90);
		sprites.push_back(temp);
	}

}

void BackGround::Update()
{
}