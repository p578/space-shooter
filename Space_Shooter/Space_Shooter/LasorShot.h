#pragma once
#ifndef LASER_SHOT_H
#define LASER_SHOT_H
#include "SpaceShooter.h"
class LasorShot : public GameObject
{
public:
	LasorShot(int _shotType, sf::Vector2f _position);
	~LasorShot();

	void Initialize() override;
	void Update() override;

	bool destory;
private:
	int shotType;
	float delay;
};

#endif // !LASER_SHOT_H



