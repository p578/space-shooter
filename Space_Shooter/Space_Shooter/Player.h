#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include "SpaceShooter.h"
#include "Hud.h"
class Player : public GameObject
{
public:
	inline static Player& Instance()
	{
		static Player instance;
		return instance;
	}
private:
	inline explicit Player() {}
	inline ~Player() {}
	inline explicit Player(Player const&) = delete;
	inline Player& operator=(Player const&) = delete;

public:
	void Initialize() override;
	void Update() override; //query input(translate, fire), query collision(take damage), health = 0(die and explode)
	void Reset();


	void SaveGame(json::JSON& _doc);
	void LoadGame(json::JSON& _doc);
	void LoadSettings(json::JSON& _doc);
	//attribut variable
	int maxPlayerLife;

	//state variable
	int playerLife;
	bool fire;
	bool explode;
	bool alive;

private:
	//attribut variable
	int maxHealth;
	float speed;
	float fireInterval;
	float driftSpeed;
	float driftRotation;
	//state variable
	int health;
	float fireTimer;
	float reviveDelay;

	void OnCollision(GameObject* _var);
	void TakeDamage(int _damage);
	void Fire();
	void Die();
	

	
};

#endif


