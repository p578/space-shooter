#include "VectorMath.h"

float VectorMath::dist(sf::Vector3f _v1, sf::Vector3f _v2)
{
	return sqrt(pow(_v2.x - _v1.x, 2) + pow(_v2.y - _v1.y, 2) + pow(_v2.z - _v1.z, 2));
}

sf::Vector3f VectorMath::Cross(sf::Vector3f _v1, sf::Vector3f _v2)
{
	float x = _v1.y * _v2.z - _v1.z * _v2.y;
	float y = _v1.z * _v2.x - _v1.x * _v2.z;
	float z = _v1.x * _v2.y - _v1.y * _v2.x;
	return sf::Vector3f(x, y, z);
}