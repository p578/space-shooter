#include "AssetManager.h"

std::map<std::string, sf::Texture*> AssetManager::textureAssets = std::map<std::string, sf::Texture*>();
std::map<std::string, sf::Font*> AssetManager::fontAssets = std::map<std::string, sf::Font*>();

AssetManager::AssetManager()
{
	defaultTexture = new sf::Texture();
	defaultFont = new sf::Font();
}

AssetManager::~AssetManager()
{
	for (auto const& var : textureAssets)
	{
		delete var.second;
	}
	for (auto const& var : fontAssets)
	{
		delete var.second;
	}
}

void AssetManager::Initialize()
{
	defaultTexture->loadFromFile("..\\Resources\\Default\\purple.jpg");
	defaultFont->loadFromFile("C:\\Windows\\Fonts\\arial.ttf");
	LoadAssets();
}

sf::Texture* AssetManager::LoadTexture(std::string _path)
{
	sf::Texture* texture = new sf::Texture();
	if (!texture->loadFromFile(_path))
	{
		texture = textureAssets.find("default")->second;
	}
	return texture;
}

sf::Font* AssetManager::LoadFont(std::string _path)
{
	sf::Font* font = new sf::Font();
	// Load it from a file
	if (!font->loadFromFile(_path))
	{
		font = fontAssets.find("default")->second;
	}
	return font;
}

const sf::Texture* AssetManager::GetTexture(std::string& _texture)
{
	auto it = textureAssets.find(_texture); //<auto> stands for:   <std::map<std::string,sf::Texture>::iterator>

	if (it != textureAssets.end())
	{
		return it->second;
	}
	return nullptr;
}

const sf::Font* AssetManager::GetFont(std::string& _font)
{
	auto it = fontAssets.find(_font); //<auto> stands for:   <std::map<std::string,sf::Font>::iterator>

	if (it != fontAssets.end())
	{
		return it->second;
	}
	return nullptr;
}

void AssetManager::LoadAssets()
{
	textureAssets.insert({ "defaultTexture",defaultTexture});
	textureAssets.insert({ "meteorBig",LoadTexture("..\\Resources\\Asteroids\\meteorBig.png") });
	textureAssets.insert({ "meteorSmall",LoadTexture("..\\Resources\\Asteroids\\meteorSmall.png")});
	textureAssets.insert({ "backgroundColor",LoadTexture("..\\Resources\\Background\\backgroundColor.png") });
	textureAssets.insert({ "starBackground",LoadTexture("..\\Resources\\Background\\starBackground.png")});
	textureAssets.insert({ "starBig",LoadTexture("..\\Resources\\Background\\starBig.png") });
	textureAssets.insert({ "starSmall",LoadTexture("..\\Resources\\Background\\starSmall.png") });
	textureAssets.insert({ "player",LoadTexture("..\\Resources\\Mainplayer\\player.png") });
	textureAssets.insert({ "playerDamaged",LoadTexture("..\\Resources\\Mainplayer\\playerDamaged.png") });
	textureAssets.insert({ "playerDamaged_2",LoadTexture("..\\Resources\\Mainplayer\\playerDamaged_2.png") });
	textureAssets.insert({ "laserGreen",LoadTexture("..\\Resources\\Mainplayer\\laserGreen.png") });
	textureAssets.insert({ "laserGreenShot",LoadTexture("..\\Resources\\Mainplayer\\laserGreenShot.png") });
	textureAssets.insert({ "enemyShip",LoadTexture("..\\Resources\\Enemies\\enemyShip.png") });
	textureAssets.insert({ "enemyUFO",LoadTexture("..\\Resources\\Enemies\\enemyUFO.png") });
	textureAssets.insert({ "laserRed",LoadTexture("..\\Resources\\Enemies\\laserRed.png") });
	textureAssets.insert({ "laserRedShot",LoadTexture("..\\Resources\\Enemies\\laserRedShot.png") });
	textureAssets.insert({ "boom3_0",LoadTexture("..\\Resources\\Misc\\boom3_0.png") });
	textureAssets.insert({ "exp2_0",LoadTexture("..\\Resources\\Misc\\exp2_0.png") });
	textureAssets.insert({ "life",LoadTexture("..\\Resources\\Hud\\life.png") });

	fontAssets.insert({ "ariel", defaultFont });
	fontAssets.insert({ "cour", LoadFont("..\\Resources\\Hud\\cour.ttf") });
	fontAssets.insert({ "courbd",LoadFont("..\\Resources\\Hud\\courbd.ttf") });
	fontAssets.insert({ "courbi",LoadFont("..\\Resources\\Hud\\courbi.ttf") });
	fontAssets.insert({ "couri",LoadFont("..\\Resources\\Hud\\couri.ttf") });
}