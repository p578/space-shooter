#include "GameEngine.h"

GameEngine::GameEngine()
{
	renderSystem = &RenderSystem::Instance();
	fileSystem = new FileSystem();
	assetManager = new AssetManager();
	inputManager = new InputManager();
	spawnManager = new SpawnManager();
	gameObjectManager = new GameObjectManager();
}

GameEngine::~GameEngine()
{

	delete fileSystem;
	delete assetManager;
	delete inputManager;
	delete spawnManager;
	delete gameObjectManager;
}

void GameEngine::Initialize()
{
	inputManager->Initialize();
	assetManager->Initialize();
	Time::Initialize();
	Physics::Initialize();
	spawnManager->Initialize();
	gameObjectManager->Initialize();
	renderSystem->Initialize();
	fileSystem->Initialize(spawnManager);
}

void GameEngine::GameLoop()
{
	while (renderSystem->GetRenderWindow()!= nullptr)
	{
		inputManager->Update();
		spawnManager->Update();
		Time::Update();
		Physics::Update();
		gameObjectManager->Update();
		renderSystem->Update();
		fileSystem->Update();
	}
}