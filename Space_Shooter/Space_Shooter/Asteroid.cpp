#include "Asteroid.h"

Asteroid::Asteroid()
{
	destory = false;
	asteroidType = 0;
	driftSpeed = sf::Vector2f(0, 0);
	rotationSpeed = 0;
	initialAngle = 0;
}

Asteroid::Asteroid(int _asteroidType, sf::Vector2f _position, float _initialAngle)
{
	destory = false;
	driftSpeed = sf::Vector2f(0, 0);
	rotationSpeed = 0;
	transform = _position;
	asteroidType = _asteroidType;
	initialAngle = _initialAngle;
}

Asteroid::~Asteroid()
{
}

void Asteroid::Initialize()
{
	std::string textureKey;
	if (asteroidType == 0)
	{
		textureKey = "meteorBig";
		tag = "bigAsteroid";
	}
	else
	{
		textureKey = "meteorSmall";
		tag = "smallAsteroid";
	}
	sf::Sprite* temp = new sf::Sprite;
	const sf::Texture* texture = AssetManager::GetTexture(textureKey);
	temp->setTexture(*texture);
	temp->setOrigin(sf::Vector2f(texture->getSize().x * 0.5f, texture->getSize().y * 0.5f));
	temp->setPosition(transform);
	temp->setRotation(initialAngle);
	sprites.push_back(temp);
	if (asteroidType == 0)
	{
		driftSpeed = sf::Vector2f(0, 35);
		rotationSpeed = 50;
		radius = 40;
	}
	else
	{
		driftSpeed = sf::Vector2f(0, 75);
		rotationSpeed = 75;
		radius = 15;
	}
}

void Asteroid::Update()
{
	sprites.front()->move(driftSpeed * Time::deltaTime);
	sprites.front()->rotate(rotationSpeed * Time::deltaTime);
	transform = sprites.front()->getPosition();
	for (GameObject* var : collision)
	{
		if (var->tag == "player")
		{
			destory = true;
		}
		if (var->tag == "playerProjectile")
		{
			Hud::Instance().point++;
			destory = true;
		}
	}
	collision.clear();
	if (transform.y > 600)
	{
		destory = true;
	}
}

void Asteroid::SaveGame(json::JSON& _doc)
{
	json::JSON asteroid = json::JSON::Object();
	asteroid["destory"] = destory;
	asteroid["asteroidType"] = asteroidType;
	GameObject::SaveGame(asteroid);
	_doc.append(asteroid);
}

void Asteroid::LoadGame(json::JSON& _doc)
{
	destory = _doc["destory"] .ToBool();
	asteroidType = _doc["asteroidType"] .ToInt();
	GameObject::LoadGame(_doc);
}

void Asteroid::LoadSettings(json::JSON& _doc)
{
}