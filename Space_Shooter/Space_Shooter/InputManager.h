#pragma once
#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "SFML/Graphics.hpp"
class InputManager
{
public:
	InputManager();
	virtual ~InputManager();

	void Initialize();
	void Update();
	static bool GetKey(std::string _key);

	static sf::RenderWindow* windowHandle;//renderSystem will give us a handle
private:
	sf::Event event;
	static bool W;
	static bool A;
	static bool S;
	static bool D;
	static bool R;
	static bool Save;
	static bool Load;
	static bool Space;
	static bool closeWindow; //renderSystem will check this flag 
};

#endif // !INPUT_MANAGER_H



