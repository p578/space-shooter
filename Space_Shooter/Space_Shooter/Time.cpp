#include "Time.h"

float Time::deltaTime = 0;
std::chrono::time_point<std::chrono::system_clock> Time::previousTime = std::chrono::system_clock::now();

void Time::Initialize()
{

}

void Time::Update()
{
	deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - previousTime).count()/1000.0f;
	previousTime = std::chrono::system_clock::now();
}