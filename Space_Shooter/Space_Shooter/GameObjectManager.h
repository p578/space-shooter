#pragma once
#ifndef GAME_OBJECT_MANAGER_H
#define GAME_OBJECT_MANAGER_H

#include <list>
#include <iterator>
#include "GameObject.h"

class GameObjectManager
{
public:
	GameObjectManager();
	virtual ~GameObjectManager();

	void Initialize();
	void Update();
	static void AddGameObject(GameObject* _gameObject);
	static void RemoveGameObject(GameObject* _gameObject);
	static void AddBackGroundElement(GameObject* _gameObject);
	static void RemoveBackGroundElement(GameObject* _gameObject);

private:
	static std::list<GameObject*> gameObjects;
	static std::list<GameObject*> backGroundElements;
	friend class RenderSystem;
	friend class FileSystem;
	friend class Physics;
};

#endif // !GAME_OBJECT_MANAGER_H



