#include "GameObject.h"

GameObject::GameObject()
{
	sprites = std::list<sf::Sprite*>();
	texts = std::list<sf::Text*>();
	transform = sf::Vector2f(0,0);
	textures = std::vector<const sf::Texture*>(); //reference assets in the assetManager
	fonts = std::vector<const sf::Font*>(); //reference assets in the assetManager
	collision = std::list<GameObject*>(); //reference game objects created by other modules
	radius = 0;
	tag = "GameObject";
}

GameObject::~GameObject()
{
	for (sf::Sprite* var : sprites)
	{
		if (var != nullptr)
		{
			delete var;
		}
	}
	for (sf::Text* var : texts)
	{
		if (var != nullptr)
		{
			delete var;
		}
	}
}

void GameObject::SaveGame(json::JSON& _doc)
{
	json::JSON gameObject = json::JSON::Object();
	gameObject["transform_x"] = transform.x;
	gameObject["transform_y"] = transform.y;
	_doc["gameObject"] = gameObject;
}

void GameObject::LoadGame(json::JSON& _doc)
{
	if (_doc.hasKey("gameObject"))
	{
		json::JSON gameObject = _doc["gameObject"];
		transform.x = gameObject["transform_x"].ToFloat();
		transform.y = gameObject["transform_y"].ToFloat();
	}
}

void GameObject::LoadSettings(json::JSON& _doc)
{
}