#include "GameObjectManager.h"

std::list<GameObject*> GameObjectManager::gameObjects = std::list<GameObject*>();
std::list<GameObject*> GameObjectManager::backGroundElements = std::list<GameObject*>();

GameObjectManager::GameObjectManager()
{
	
}

GameObjectManager::~GameObjectManager()
{
	for (GameObject* var : gameObjects)
	{
		delete var;
	}
	for (GameObject* var : backGroundElements)
	{
		delete var;
	}
}

void GameObjectManager::Initialize()
{
	//for (GameObject* var : backGroundElements)
	//{
	//	var->Initialize();
	//}
	//for (GameObject* var : gameObjects)
	//{
	//	var->Initialize();
	//}
}

void GameObjectManager::Update()
{
	for (GameObject* var : backGroundElements)
	{
		var->Update();
	}
	for (GameObject* var : gameObjects)
	{
		var->Update();
	}
}

void GameObjectManager::AddGameObject(GameObject* _gameObject)
{
	_gameObject->Initialize();
	gameObjects.push_back(_gameObject);
}

void GameObjectManager::RemoveGameObject(GameObject* _gameObject)
{
	gameObjects.remove(_gameObject);
}

void GameObjectManager::AddBackGroundElement(GameObject* _gameObject)
{
	_gameObject->Initialize();
	backGroundElements.push_back(_gameObject);
}

void GameObjectManager::RemoveBackGroundElement(GameObject* _gameObject)
{
	backGroundElements.remove(_gameObject);
}