#pragma once
#ifndef BACKGROUND_H
#define BACKGROUND_H
#include "SpaceShooter.h"

#define SPRITE_SIZE 256

class BackGround: public GameObject
{
public:
	BackGround();
	~BackGround();

	void Initialize() override;
	void Update() override;
};

#endif // !BACKGROUND_H

