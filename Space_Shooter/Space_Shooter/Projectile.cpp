#include "Projectile.h"


Projectile::Projectile()
{
	destory = false;
	projectileScource = 0;
	speed = 0;
	heading = sf::Vector2f(0,0);
}

Projectile::Projectile(int _projectileScource, sf::Vector2f _position, sf::Vector2f _heading)
{
	destory = false;
	speed = 0;
	heading = _heading;
	transform = _position;
	projectileScource = _projectileScource;
}

Projectile::~Projectile()
{
}

void Projectile::Initialize()
{
	
	radius = 9;
	destory = false;
	std::string spriteKey;
	if (projectileScource)
	{
		spriteKey = "laserRed";
		tag = "enemyProjectile";
		speed = 100;
	}
	else
	{
		spriteKey = "laserGreen";
		tag = "playerProjectile";
		speed = 300;
	}
	sf::Sprite* temp = new sf::Sprite;
	const sf::Texture* texture = AssetManager::GetTexture(spriteKey);
	textures.push_back(texture);
	temp->setTexture(*texture);
	temp->setPosition(transform);
	temp->setOrigin(sf::Vector2f(texture->getSize().x * 0.5f, texture->getSize().y * 0.5f));
	sprites.push_back(temp);
	Aim(heading);
}

void Projectile::Update()
{

	sprites.front()->move(heading * speed * Time::deltaTime);
	transform = sprites.front()->getPosition();

	for (GameObject* var : collision)
	{
		if ((var->tag != "playerProjectile") && (var->tag != "enemyProjectile"))
		{
			if ((projectileScource == 0 && var->tag != "player") || (projectileScource == 1 && var->tag == "player"))
			{
				OnCollision();
			}
		}
	}

	if (transform.y <= -400|| transform.x <= -300|| transform.y >= 400|| transform.x >= 300)
	{
		destory = true;
	}
}

void Projectile::OnCollision()
{
	destory = true;
}

void Projectile::Aim(sf::Vector2f _heading)
{
	sf::Vector3f _heading_v3 = sf::Vector3f(_heading.x, _heading.y, 0);
	sf::Vector3f defaultDirection = sf::Vector3f(0, -1, 0);
	float cosine = _heading.x * defaultDirection.x + _heading.y * defaultDirection.y;
	if (VectorMath::Cross(_heading_v3, defaultDirection).z<0)
	{
		sprites.front()->setRotation(acos(cosine) * 180 / 3.14f);
	}
	else
	{
		sprites.front()->setRotation(-acos(cosine) * 180 / 3.14f);
	}
}

void Projectile::SaveGame(json::JSON& _doc)
{
	json::JSON projectile = json::JSON::Object();
	projectile["destory"] = destory;
	projectile["projectileScource"] = projectileScource;
	projectile["heading_x"] = heading.x;
	projectile["heading_y"] = heading.y;
	GameObject::SaveGame(projectile);
	_doc.append(projectile);
}

void Projectile::LoadGame(json::JSON& _doc)
{
	destory = _doc["destory"] .ToBool();
	projectileScource = _doc["projectileScource"] .ToInt();
	heading.x = _doc["heading_x"] .ToFloat();
	heading.y = _doc["heading_y"] .ToFloat();
	GameObject::SaveGame(_doc);
}

void Projectile::LoadSettings(json::JSON& _doc)
{
}