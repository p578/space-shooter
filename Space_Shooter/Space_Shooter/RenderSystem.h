#pragma once
#ifndef RENDER_SYSTEM_H
#define RENDER_SYSTEM_H

#define WINDOW_WIDTH 512
#define WINDOW_HEIGHT 768

#include "SFML\Graphics.hpp"
#include <list>
#include <iterator>
#include "GameObjectManager.h"

class RenderSystem
{

public:
	inline static RenderSystem& Instance()
	{
		static RenderSystem instance;
		return instance;
	}

	void Initialize();
	void Update();
	inline sf::RenderWindow* GetRenderWindow() { return window; }
	inline void CloseWindow() { window->close(); delete window; window = nullptr; }

private:
	inline explicit RenderSystem() { window = new sf::RenderWindow(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Space Shooter"); }
	inline ~RenderSystem() { if (window != nullptr) { delete window; } }
	inline explicit RenderSystem(RenderSystem const&) = delete;
	inline RenderSystem& operator=(RenderSystem const&) = delete;

	sf::RenderWindow* window;
};

#endif // !RENDER_SYSTEM_H



