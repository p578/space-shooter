#pragma once
#ifndef FILE_SYSTEM_H
#define FILE_SYSTEM_H

#define SAVE_FILE "file.json"
#define LOAD_FILE "file.json"

#include "GameObjectManager.h"
#include "SpawnManager.h"
#include "json.hpp"
#include <fstream>

class FileSystem
{
public:
	FileSystem();
	~FileSystem();

	void Initialize(SpawnManager* _spawnManager);
	void Update();
	void Load(std::string _path);
	void Save(std::string _path);

private:
	SpawnManager* spawnManager;
	json::JSON document;
};

#endif


