#include "FileSystem.h"
FileSystem::FileSystem()
{
}

FileSystem::~FileSystem()
{
}

void FileSystem::Initialize(SpawnManager* _spawnManager)
{
	spawnManager = _spawnManager;
}

void FileSystem::Update()
{
	if (InputManager::GetKey("~"))
		Save(SAVE_FILE);

	if (InputManager::GetKey("L"))
		Load(LOAD_FILE);
}

void FileSystem::Save(std::string _path)
{
	json::JSON SpaceShooter = json::JSON::Object();
	spawnManager->SaveGame(SpaceShooter);
	document["SpaceShooter"] = SpaceShooter;
	std::ofstream ofs(_path);
	ofs << document.dump() << std::endl;
}

void FileSystem::Load(std::string _path)
{
	std::ifstream inputStream("file.json"); 
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>()); 
	json::JSON document = json::JSON::Load(str);
	if (document.hasKey("SpaceShooter"))
	{
		json::JSON SpaceShooter = document["SpaceShooter"];
		spawnManager->LoadGame(SpaceShooter);
	}
}