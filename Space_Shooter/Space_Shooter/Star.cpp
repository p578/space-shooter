#include "Star.h"

Star::Star(int _starType,sf::Vector2f _position)
{
	transform = _position;
	starType = _starType;
}
Star::~Star()
{
}

void Star::Initialize()
{
	std::string spriteKey;
	if (starType == 0)
	{
		spriteKey = "starBig";
	}
	else
	{
		spriteKey = "starSmall";
	}
	sf::Sprite* temp = new sf::Sprite;
	temp->setTexture(*AssetManager::GetTexture(spriteKey));
	temp->setPosition(transform);
	sprites.push_back(temp);
	if (starType == 0)
	{
		driftSpeed = sf::Vector2f(0, 50);
	}
	else
	{
		driftSpeed = sf::Vector2f(0, 25);
	}
}

void Star::Update()
{
	sprites.front()->move(driftSpeed * Time::deltaTime);
	transform = sprites.front()->getPosition();
}