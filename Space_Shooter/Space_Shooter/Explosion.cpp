#include "Explosion.h"

Explosion::Explosion(int _explosionType, sf::Vector2f _position)
{
	explosionType = _explosionType;
	transform = _position;
}

Explosion::~Explosion()
{
}


void Explosion::Initialize()
{
	destory = false;
	std::string spriteKey;
	sf::Sprite* temp = new sf::Sprite;
	if (explosionType)
	{
		spriteKey = "exp2_0";
		frameCount = 16;
	}
	else
	{
		spriteKey = "boom3_0";
		frameCount = 64;
	}
	const sf::Texture* texture = AssetManager::GetTexture(spriteKey);
	temp->setTexture(*texture);
	temp->setTextureRect(sf::IntRect(0, 0, 64, 64));
	temp->setPosition(transform);
	temp->setOrigin(sf::Vector2f(32, 32));
	sprites.push_back(temp);
}

void Explosion::Update()
{
	if (frameCount > 0) 
	{
		if (explosionType)
		{
			int row = 3 - (frameCount - 1) / 4;
			int column = 3 - (frameCount - 1) % 4;
			sprites.front()->setTextureRect(sf::IntRect(column * 64, row * 64, 64, 64));
			frameCount--;
		}
		else
		{
			int row = 7 - (frameCount - 1) / 8;
			int column = 7 - (frameCount - 1) % 8;
			sprites.front()->setTextureRect(sf::IntRect(column * 64, row * 64, 64, 64));
			frameCount--;
		}
	}
	else
	{
		destory = true;
	}
}