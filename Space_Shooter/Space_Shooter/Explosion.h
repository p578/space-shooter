#pragma once
#ifndef EXPLOSION_H
#define EXPLOSION_H
#include "SpaceShooter.h"


class Explosion :public GameObject
{
public:
	Explosion(int _explosionType, sf::Vector2f _position);
	~Explosion();

	void Initialize() override;
	void Update() override;

	bool destory;
private:
	int explosionType;
	int frameCount;
};

#endif // !EXPLOSION_H



