#pragma once
#ifndef ASSET_MANAGER_H
#define ASSET_MANAGER_H


#include <map>
#include <iterator>
#include <SFML/Graphics.hpp>

class AssetManager
{
public:
	AssetManager();
	virtual ~AssetManager();

	void Initialize();
	static const sf::Texture* GetTexture(std::string& _texture);
	static const sf::Font* GetFont(std::string& _font);

	

private:
	void LoadAssets();//Later it needs to take a string path parameter
	sf::Texture* LoadTexture(std::string _path);
	sf::Font* LoadFont(std::string _path);
	static std::map<std::string, sf::Texture*> textureAssets;
	static std::map<std::string, sf::Font*> fontAssets;
	sf::Texture* defaultTexture;
	sf::Font* defaultFont;
};

#endif // !ASSET_MANAGER_H



