#pragma once
#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "SpaceShooter.h"

class Projectile : public GameObject
{
public:
	Projectile();
	Projectile(int _projectileScource, sf::Vector2f _position, sf::Vector2f _heading);
	~Projectile();

	void Initialize() override;
	void Update() override; //move forward(translate), query collision(self destruct and play Shot fx)


	void SaveGame(json::JSON& _doc);
	void LoadGame(json::JSON& _doc);
	void LoadSettings(json::JSON& _doc);

	//state variable
	bool destory;
	int projectileScource;
private:
	sf::Vector2f heading;

	//attribute variable
	float speed;

	void OnCollision();
	void Aim(sf::Vector2f _heading);

};

#endif

