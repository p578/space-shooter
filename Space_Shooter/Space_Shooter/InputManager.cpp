#include "InputManager.h"
#include <iostream>
sf::RenderWindow* InputManager::windowHandle=nullptr;
bool InputManager::W = false;
bool InputManager::A = false;
bool InputManager::S = false;
bool InputManager::D = false;
bool InputManager::R = false;
bool InputManager::Save = false;
bool InputManager::Load = false;
bool InputManager::Space = false;
bool InputManager::closeWindow = false;

InputManager::InputManager()
{
}

InputManager::~InputManager()
{
}

void InputManager::Initialize()
{
}

void InputManager::Update()
{

    while (windowHandle->pollEvent(event))
    {
        switch (event.type)
        {
        case sf::Event::Closed:
            closeWindow = true;
            break;
        case sf::Event::KeyPressed:
            if (event.key.code == sf::Keyboard::W)
            {
                W = true;
            }
            if (event.key.code == sf::Keyboard::A)
            {
                A = true;
            }
            if (event.key.code == sf::Keyboard::S)
            {
                S = true;
            }
            if (event.key.code == sf::Keyboard::D)
            {
                D = true;
            }
            if (event.key.code == sf::Keyboard::R)
            {
                R = true;
            }
            if (event.key.code == sf::Keyboard::Tilde)
            {
                Save = true;
            }
            if (event.key.code == sf::Keyboard::L)
            {
                Load = true;
            }
            if (event.key.code == sf::Keyboard::Space)
            {
                Space = true;
            }
            if (event.key.code == sf::Keyboard::Escape)
            {
                closeWindow = true;
            }
            break;
        case sf::Event::KeyReleased:
            if (event.key.code == sf::Keyboard::W)
            {
                W = false;
            }
            if (event.key.code == sf::Keyboard::A)
            {
                A = false;
            }
            if (event.key.code == sf::Keyboard::S)
            {
                S = false;
            }
            if (event.key.code == sf::Keyboard::D)
            {
                D = false;
            }
            if (event.key.code == sf::Keyboard::R)
            {
                R = false;
            }
            if (event.key.code == sf::Keyboard::Tilde)
            {
                Save = false;
            }
            if (event.key.code == sf::Keyboard::L)
            {
                Load = false;
            }
            if (event.key.code == sf::Keyboard::Space)
            {
                Space = false;
            }
            if (event.key.code == sf::Keyboard::Escape)
            {
                closeWindow = true;
            }
            break;
        case sf::Event::MouseButtonPressed:
            if (event.mouseButton.button == sf::Mouse::Left)
            {
            }
            break;
        default:
            break;
        }
    }
}

bool InputManager::GetKey(std::string _key)
{
    if (_key == "W")
    {
        return W;
    }
    if (_key == "A")
    {
        return A;
    }
    if (_key == "S")
    {
        return S;
    }
    if (_key == "D")
    {
        return D;
    }
    if (_key == "R")
    {
        return R;
    }
    if (_key == "~")
    {
        return Save;
    }
    if (_key == "L")
    {
        return Load;
    }
    if (_key == "Space")
    {
        return Space;
    }
    if (_key == "CloseWindow")
    {
        return closeWindow;
    }
}