#include "RenderSystem.h"

void RenderSystem::Initialize()
{
	window->setFramerateLimit(60);
	sf::View view;
	float halfWidth = window->getSize().x * 0.5f;
	float halfHeight = window->getSize().y * 0.5f;
	view.reset(sf::FloatRect(-halfWidth, -halfHeight, halfWidth * 2.0f, halfHeight * 2.0f));
	window->setView(view);
	InputManager::windowHandle = window;
}

void RenderSystem::Update()
{
	if (window != nullptr)
	{

		window->clear();

		for (GameObject* var : GameObjectManager::backGroundElements)
		{
			for (sf::Sprite* sprite : var->sprites)
			{
				if (sprite != nullptr)
				{
					window->draw(*sprite);
				}
			}
			for (sf::Text* text : var->texts)
			{
				if (text != nullptr)
				{
					window->draw(*text);
				}
			}

		}

		for (GameObject* var : GameObjectManager::gameObjects)
		{
			for (sf::Sprite* sprite : var->sprites)
			{
				if (sprite != nullptr)
				{
					window->draw(*sprite);
				}
			}
			for (sf::Text* text : var->texts)
			{
				if (text != nullptr)
				{
					window->draw(*text);
				}
			}
		}
	}
	window->display();

	if (InputManager::GetKey("CloseWindow"))
	{
		CloseWindow();
	}
}


