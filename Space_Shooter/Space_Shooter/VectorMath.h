#pragma once
#ifndef VECTOR_MATH_H
#define VECTOR_MATH_H
#include "GameObjectManager.h"
#include <cmath>

class VectorMath
{
public:
	static sf::Vector3f Cross(sf::Vector3f _v1, sf::Vector3f _v2);
	static float dist(sf::Vector3f _v1, sf::Vector3f _v2);

		
};

#endif // !VECTOR_MATH_H


