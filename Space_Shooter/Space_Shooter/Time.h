#pragma once
#ifndef TIME_H
#define TIME_H

#include <chrono>
class Time //most importantly deltaTime
{
public:
	static void Initialize();
	static void Update();

	static float deltaTime;

private:
	static std::chrono::time_point<std::chrono::system_clock> previousTime;
};

#endif


