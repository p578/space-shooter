#include "LasorShot.h"

LasorShot::LasorShot(int _shotType, sf::Vector2f _position)
{
	shotType = _shotType;
	transform = _position;
}

LasorShot::~LasorShot()
{
}

void LasorShot::Initialize()
{
	delay = 0.1f;
	destory = false;
	std::string spriteKey;
	if (shotType)
	{
		spriteKey = "laserRedShot";
	}
	else
	{
		spriteKey = "laserGreenShot";
	}
	sf::Sprite* temp = new sf::Sprite;
	const sf::Texture* texture = AssetManager::GetTexture(spriteKey);
	temp->setTexture(*texture);
	temp->setPosition(transform);
	temp->setOrigin(sf::Vector2f(texture->getSize().x * 0.5f, texture->getSize().y * 0.5f));
	sprites.push_back(temp);
}

void LasorShot::Update()
{
	if (delay > 0)
	{
		delay -= Time::deltaTime;
	}
	else
	{
		destory = true;
	}
}