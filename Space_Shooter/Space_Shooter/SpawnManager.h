#pragma once
#ifndef SPAWN_MANAGER_H
#define SPAWN_MANAGER_H

#include "GameObjectIncludes.h"
#include "GameObjectManager.h"
#include "Time.h"
#include <list>
#include <vector>
#include <stdlib.h>     

class SpawnManager
{
public:
	SpawnManager();
	~SpawnManager();

	void Initialize();
	void Update();

	void SaveGame(json::JSON& _doc);
	void LoadGame(json::JSON& _doc);
	void LoadSettings(json::JSON& _doc);
private:
	//state variable
	int spawnPointIndex;

	//attributes
	float starSpawnTimer;
	float starSpawnInterval;
	float asteroidSpawnTimer;
	float asteroidSpawnInterval;
	float enemySpawnTimer;
	float enemySpawnInterval;

	BackGround* backGround;
	Player* player;
	Hud* hud;
	std::vector<sf::Vector2f> spawnPoints;
	std::list<Star*> stars;
	std::list<Asteroid*> asteroids;
	std::list<Projectile*> projectiles;
	std::list<LasorShot*> lasorShots;
	std::list<Explosion*> explosions;
	std::list<Enemy*> enemies;

	void RandomSpawnPoint(int _min, int _max);
	void clearInteractables();


	void SpawnStar(int _index);
	std::list<Star*>::iterator RemoveStar(std::list<Star*>::iterator _it);
	void SpawnAsteroid(int _index);
	std::list<Asteroid*>::iterator RemoveAsteroid(std::list<Asteroid*>::iterator _it);
	void SpawnEnemy(int _index);
	std::list<Enemy*>::iterator RemoveEnemy(std::list<Enemy*>::iterator _it);

	void Restart();
	

};


#endif // !SPAWN_MANAGER_H


