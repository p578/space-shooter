#pragma once
#ifndef STAR_H
#define STAR_H

#include "SpaceShooter.h"

class Star : public GameObject
{
public:
	Star(int _starType, sf::Vector2f _position);
	~Star();

	void Initialize();
	void Update();

private:
	int starType;
	sf::Vector2f driftSpeed;
};
#endif


