#include "Enemy.h"
#include <cmath>

Enemy::Enemy()
{
	fire = false;
	destory = false;
	heading = sf::Vector2f(0,0);
	health = 0;
	fireTimer = 0;
	selfDestructTimer = 0;
	enemyType = 0;
	attackMode = false;

	maxHealth = 0;
	fireInterval = 0;
	speed = 0;
	driftSpeed = 0;
}

Enemy::Enemy(int _enemyType, sf::Vector2f _position)
{
	fire = false;
	destory = false;
	heading = sf::Vector2f(0, 0);
	health = 0;
	fireTimer = 0;
	enemyType = _enemyType;
	selfDestructTimer = 0;
	attackMode = false;

	maxHealth = 0;
	fireInterval = 0;
	speed = 0;
	driftSpeed = 0;

	transform = _position;
}

Enemy::~Enemy()
{
	
}

void Enemy::Initialize()
{

	driftSpeed = 35;
	radius = 25;
	heading = sf::Vector2f(0,1);
	attackMode = false;
	std::string textureKey;
	tag = "enemy";

	if (enemyType == 0)
	{
		textureKey = "enemyUFO";
		maxHealth = 1;
		speed = 200;
		fireInterval = 0;
		fireTimer = fireInterval;
		selfDestructTimer = 5;
	}
	else
	{
		textureKey = "enemyShip";
		maxHealth = 2;
		speed = 100;
		fireInterval = 3;
		fireTimer = fireInterval;
		selfDestructTimer = 0;
	}
	health = maxHealth;
	sf::Sprite* temp = new sf::Sprite;
	const sf::Texture* texture = AssetManager::GetTexture(textureKey);
	temp->setTexture(*texture);
	temp->setOrigin(sf::Vector2f(texture->getSize().x * 0.5f, texture->getSize().y * 0.5f));
	temp->setPosition(transform);
	sprites.push_back(temp);
	heading = TrackPlayer();
}

void Enemy::Update()
{
	heading = TrackPlayer();
	if (enemyType == 0)
	{
		if (selfDestructTimer>0)
		{
			sprites.front()->move((heading * speed) * Time::deltaTime);
			selfDestructTimer -= Time::deltaTime;
		}
		else
		{
			Die();
		}
	}
	else 
	{
		if (RangePlayer() < 300)
		{
			attackMode = true;
		}
		if (attackMode)
		{
			sprites.front()->move(sf::Vector2f(0, 1) * driftSpeed * Time::deltaTime);
			Fire();
		}
		else
		{
			sprites.front()->move((heading * speed) * Time::deltaTime);
		}
	}
	transform = sprites.front()->getPosition();
	if (transform.y > 600)
	{
		Die();
	}

	if (health <= 0)
	{
		Hud::Instance().point += 2;
		Die();
	}

	for (GameObject* var : collision)
	{
		OnCollision(var);
	}
	collision.clear();
}

void Enemy::OnCollision(GameObject* _var)
{
	if (_var->tag == "player")
	{
		Die();
	}
	else if (_var->tag == "playerProjectile")
	{
		TakeDamage(1);
	}
}

void Enemy::TakeDamage(int _damage)
{
	health -= _damage;
}


sf::Vector2f Enemy::TrackPlayer()
{
	sf::Vector2f playerVector = Player::Instance().transform-transform;
	float distance = sqrt(pow(playerVector.x,2)+pow(playerVector.y,2));
	playerVector = playerVector / distance;
	sf::Vector3f playerVector_v3 = sf::Vector3f(playerVector.x,playerVector.y,0);
	sf::Vector3f defaultDirection = sf::Vector3f(0,1,0);
	float cosine = playerVector.x * defaultDirection.x + playerVector.y * defaultDirection.y;
	if (VectorMath::Cross(playerVector_v3, defaultDirection).z<0)
	{
		sprites.front()->setRotation(acos(cosine) * 180 / 3.14f);
	}
	else
	{
		sprites.front()->setRotation(-acos(cosine) * 180 / 3.14f);
	}
	return playerVector;
}

float Enemy::RangePlayer()
{
	sf::Vector2f playerVector = Player::Instance().transform - transform;
	float distance = sqrt(pow(playerVector.x, 2) + pow(playerVector.y, 2));
	return distance;
}

void Enemy::Fire()
{
	if (fireTimer >= fireInterval)
	{
		fire = true;
		fireTimer = 0;
	}
	fireTimer += Time::deltaTime;
}

void Enemy::Die()
{
	destory = true;
}

void Enemy::SaveGame(json::JSON& _doc)
{
	json::JSON enemy = json::JSON::Object();
	enemy["fire"]  = fire;
	enemy["destory"]  = destory;
	enemy["heading_x"] = heading.x;
	enemy["heading_y"] = heading.y;
	enemy["health"] = health;
	enemy["fireTimer"] = fireTimer;
	enemy["selfDestructTimer"] = selfDestructTimer;
	enemy["enemyType"] = enemyType;
	enemy["attackMode"] = attackMode;
	GameObject::SaveGame(enemy);
	_doc.append(enemy);
}

void Enemy::LoadGame(json::JSON& _doc)
{
	fire = _doc["fire"].ToBool();
	destory = _doc["destory"] .ToBool();
	heading.x = _doc["heading_x"].ToFloat();;
	heading.y = _doc["heading_y"] .ToFloat();
	health = _doc["health"].ToInt();
	fireTimer = _doc["fireTimer"].ToFloat();
	selfDestructTimer = _doc["selfDestructTimer"] .ToFloat();
	enemyType = _doc["enemyType"].ToInt();
	attackMode = _doc["attackMode"] .ToBool();
	GameObject::LoadGame(_doc);
}

void Enemy::LoadSettings(json::JSON& _doc)
{
}