# Space Shooter

<br>Project 1 for PROG50016 Game Architecture
<br>This is a topdown shooter game programmed using  SFML framework
<br>W,A,S,D for movement, Space for fire
<br>
<br>![Screenshot](/images/1.JPG)
<br>![Screenshot](/images/3.JPG)
<br>![Screenshot](/images/2.JPG)
